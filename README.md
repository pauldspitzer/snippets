# README #

Handful of (hopefully) useful snippets

## merge.js ##

Illustrates how to merge two disparate result sets with paging support.

## FitnessProportionateCollection.js ##

The FitnessProportionateCollection class illustrates how to perform fitness proportionate selection in JavaScript. See https://en.wikipedia.org/wiki/Fitness_proportionate_selection. Note, this could be implemented as a utility method that accepts an Array as input (see below). That said, I find that because the implementation requires sorting of the Array, the object model is more suitable and ensures there is no unexpected alteration of the original array if it existed. Additionally, the collection class, in this case, allows us to update the total weight when items are added and removed, preventing the need for an additional loop when it comes time to perform the selection. As always, specific requirements and project nuances should inform the implementation details and approach taken.

### Example ###

```
// simplified example of a FitnessProportionateCollection used to randomly select from a playlist
// the rating property determines the probability that the item will be selected

import FitnessProportionateCollection from './FitnessProportionateCollection.mjs'

var playlist = new FitnessProportionateCollection( 'rating' );

playlist.add({ title: 'My Favorite Song', rating: 5 });
playlist.add({ title: 'Another Favorite Song', rating: 5 });
playlist.add({ title: 'A Good Song', rating: 3 });
playlist.add({ title: 'Another Good Song', rating: 3 });
playlist.add({ title: 'A Great Song', rating: 4 });
playlist.add({ title: 'An OK Song', rating: 1 });
playlist.add({ title: 'Another OK Song', rating: 1 });
playlist.add({ title: 'And Another OK Song', rating: 1 });

var songToPlay = playlist.selectProportionate();

```

## weightedRandomSelection.js ##

An example of fitness proportionate selection in JavaScript implemented as a single utility function.

### Example ###

```
var playlist = [];

playlist.push({ title: 'My Favorite Song', rating: 5 });
playlist.push({ title: 'Another Favorite Song', rating: 5 });
playlist.push({ title: 'A Good Song', rating: 3 });
playlist.push({ title: 'Another Good Song', rating: 3 });
playlist.push({ title: 'A Great Song', rating: 4 });
playlist.push({ title: 'An OK Song', rating: 1 });
playlist.push({ title: 'Another OK Song', rating: 1 });
playlist.push({ title: 'And Another OK Song', rating: 1 });

var songToPlay = weightedRandomSelection( playlist, 'rating' );

```