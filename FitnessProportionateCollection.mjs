/**
 * Utiltiy method, sorts an array of objects by a property within the object
 * @private
 */
function keySort( key, direction ){
	return function( item1, item2 ){
		var a = ( direction == null || direction == 1 ) ? item1 : item2;
		var b = ( a == item1 ) ? item2 : item1;
		return a[ key ] - b[ key ];
	};
}

/**
 * Updates the probabilities of selection across the collection
 * @private
 */
function updateProbabilities(){
	// reset probabilities array
	this.itemProbabilities = [];

	this.items.forEach(( item, i ) =>  {
		// create the object to store the probability
		this.itemProbabilities[ i ] = {};

		// set the probability the item will be selected
		this.itemProbabilities[ i ].probability = item[ this.fitnessParamter ] / this.totalWeight;

		// get the previous item's cumulative probability or zero
		var priorCumulativeProbability = ( i == 0 ) ? 0 : this.itemProbabilities[ i - 1 ].cumulativeProbability;

		// set the item's cumulative probablity by adding the previous cumulative probability to the probablity
		this.itemProbabilities[ i ].cumulativeProbability = this.itemProbabilities[ i ].probability + priorCumulativeProbability;
	});
}

/**
 *
 */
class FitnessProportionateCollection {

	/**
	 * Constructs a new FitnessProportionateCollection
	 * @param fitnessParamter {String} Specifies the name of the property that will be used 
	 * to determine the weighted random selection. This property must be set as a property of
	 * every item that is added to the collection. If it is not provided the default value is 1.
	 * @example var collection = new FitnessProportionateCollection( 'weight' );
	 * var collection = new FitnessProportionateCollection( 'rating' );
	 */
	constructor( fitnessParamter ) {
		this.fitnessParamter = fitnessParamter;
		this.items = [];
		this.totalWeight = 0;
	}

	/**
	 * Adds an item to the collection
	 * @param item {Object} The object to add. The object must have a property that matches 
	 * the provided fitnessParamter specified in the constructor of the FitnessProportionateCollection. 
	 * If no such property exists it will be added with a value of 1. Note, if the value is zero the 
	 * item will never be chosen when calling the selectProportionate method.
	 * @example
	 * var collection = new FitnessProportionateCollection( 'rating' );
	 * collection.add({ title: 'My Favorite Song', rating: 5 });
	 * collection.add({ title: 'Some Other Song', rating: 1 });
	 */
	add( item ){
		// if the item does not have a property matching the fitnessParameter, add one and set the default value to 1
		if( item[ this.fitnessParamter ] == null ) item[ this.fitnessParamter ] = 1;

		// update the total weight which is used to sort and determine probabilities
		this.totalWeight += item[ this.fitnessParamter ];

		// add the item to the internal array
		this.items.push( item );

		// sort the internal array by the fitness parameter
		this.items.sort( keySort( this.fitnessParamter ));
	}

	/**
	 * Removes an item from the collection
	 * @param item {Object} The item to remove.
	 */
	remove( item ){
		// find the item in the internal array
		var index = this.items.indexOf( item );

		// if it exists, remove it
		if( index != -1 ){

			// decrement the total weight by the item's fitnessParameter
			this.totalWeight -= item[ this.fitnessParamter ];

			// remove the item
			this.items.splice( index, 1 );
		}
	}

	/**
	 * @return {Boolean} Whether or not the collection contains a specified item.
	 */
	hasItem( item ){
		var index = this.items.indexOf( item );
		return ( index != -1 );
	}

	/**
	 * Performs a random weighted selction from the collection.
	 */
	selectProportionate(){
		// update the probabilities
		updateProbabilities.apply( this );

		// select a random number
		var ran = Math.random();

		// find the index to select using the random number
		var index = 0;
		for( var i = 0; i < this.items.length; i++ ){
			var item = this.items[ i ];
			if( ran <= this.itemProbabilities[ i ].cumulativeProbability ){
				index = i;
				break;
			}
		};
		return this.items[ index ];
	}

	/**
	 * @return {Number} The number of items in the collection
	 */
	get length(){
		return this.items.length;
	}

}

export default FitnessProportionateCollection;
