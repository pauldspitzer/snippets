import { default as assert } from 'assert';
import FitnessProportionateCollection from '../FitnessProportionateCollection.mjs'

var a = { value: "A", weight: 1 };
var b = { value: "B", weight: 3 };
var c = { value: "C", weight: 2 };
var d = { value: "D", weight: 1 };
var e = { value: "E", weight: 0 };
var f = { value: "F", weight: 0 };


describe( 'FitnessProportionateCollection', function() {
	describe( 'add', function() {
		let collection = new FitnessProportionateCollection( 'weight' );
		collection.add( a );
		collection.add( b );
		it( 'add two items to the collection, length should be 2', function() {
			assert.equal( collection.length, 2 );
		});
	});
	describe( 'remove', function() {
		let collection = new FitnessProportionateCollection( 'weight' );
		collection.add( a );
		collection.add( b );
		collection.remove( a );
		it( 'add two items to the collection, remove one, length should be 1', function() {
			assert.equal( collection.length, 1 );
		});
		it( 'collection should no longer have the removed item', function() {
			assert.equal( collection.hasItem( a ), false );
		});
	});
	describe( 'hasItem', function() {
		let collection = new FitnessProportionateCollection( 'weight' );
		collection.add( d );
		it( 'collection has the item added', function() {
			assert.equal( collection.hasItem( d ), true );
		});
	});
	describe( 'selectProportionate', function() {
		let collection = new FitnessProportionateCollection( 'weight' );
		collection.add( a );
		collection.add( b );
		collection.add( c );
		var item = collection.selectProportionate();
		it( 'random item selected', function() {
			assert.notEqual( item, null );
		});
	});
	describe( 'selectProportionate with guaranteed outcome', function() {
		let collection = new FitnessProportionateCollection( 'weight' );
		collection.add( d );
		collection.add( e );
		collection.add( f );
		var item = collection.selectProportionate();
		it( 'expected item selected', function() {
			assert.equal( item, d );
		});
	});
});
