/**
 * Peforms a random, weighted selection (fitness proportionate selection) of an item from the array.
 * @param collection {Array} The array to select an item from
 * @param fitnessParameter {String} The property used to weight the selection
 */
function weightedRandomSelection( collection, fitnessParamter ){
	// first duplicate the array so we do not alter the sort order of the original
	var items = collection.slice( 0 );

	// create an array to store the probabilities
	var itemProbabilities = [];

	// we need the total weight of all items
	var totalWeight = 0;
	items.forEach( function( item, i ){
		totalWeight += item[ fitnessParamter ];
	});

	// sort the array
	items.sort( function( item1, item2 ){
		return item1[ fitnessParamter ] - item2[ fitnessParamter ];
	});

	// now calculate the probablities
	items.forEach( function( item, i ){
		// create the object to store the probabilities
		itemProbabilities[ i ] = {};

		// set the probability the item will be selected
		//item.__probability = item[ fitnessParamter ] / totalWeight;
		itemProbabilities[ i ].probability = item[ fitnessParamter ] / totalWeight;

		// get the previous item's cumulative probability or zero
		var priorCumulativeProbability = ( i == 0 ) ? 0 : itemProbabilities[ i - 1 ].cumulativeProbability;

		// set the item's cumulative probablity by adding the previous cumulative probability to the probablity
		itemProbabilities[ i ].cumulativeProbability = itemProbabilities[ i ].probability + priorCumulativeProbability;
	});

	// perform the selection
	var ran = Math.random();
	var index = 0;
	for( var i = 0; i < items.length; i++ ){
		var item = items[ i ];
		if( ran <= itemProbabilities[ i ].cumulativeProbability ){
			index = i;
			break;
		}
	};

	return items[ index ];
};