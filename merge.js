/**
 * The number of results to generate per search
 */
var totalRecords = 100;

/**
 * The number of results to display per page
 */
var pageSize = 10;

/**
 * The current page index for the first result set
 */
var pageIndex1 = 0;

/**
 * The current page index for the second result set
 */
var pageIndex2 = 0;

/**
 * Holds the first test result set
 */
var searchResultSet1 = [];

/**
 * Holds the second test result set
 */
var searchResultSet2 = [];

/**
 * Utiltiy method, sorts an array of objects by a property within the object
 */
function keySort( key, direction ){
	return function( item1, item2 ){
		var a = ( direction == null || direction == 1 ) ? item1 : item2;
		var b = ( a == item1 ) ? item2 : item1;
		return a[ key ] - b[ key ];
	};
}

/**
 * Populates test data sets. The 's' property is used for sorting the result sets
 */
function populateResults(){
	for( var i = 0; i < totalRecords; i++ ){
		var sortIndex = Math.round( Math.random() * 100 );
		if( i / 3 != Math.round( i / 3 )){
			searchResultSet1.push({
				id: i,
				s: sortIndex,
				type: 1
			})
		} else {
			searchResultSet2.push({
				id: i,
				s: sortIndex,
				type: 2
			})
		}
	}

	searchResultSet1.sort( keySort( 's' ));
	searchResultSet2.sort( keySort( 's' ));
}

/**
 * Mimics retrieving a page of results from the specified collection.
 */
function getResults( collection, startIndex ){
	return collection.slice( startIndex, pageSize );
}

/**
 * Gets the next page of results
 */
function getNext(){
	console.info( 'getNext', pageIndex1, pageIndex2 );

	// query the api for orders
	var results1 = getResults( searchResultSet1, pageIndex1 );

	// sfcc search for orders
	var results2 = getResults( searchResultSet2, pageIndex2 );
	
	// concat the results, sort, and pull the page size
	var results = results1.concat( results2 ).sort( keySort( 's' )).splice( 0, pageSize );
	
	// determine the current indices for retrieving the next page of results
	// this can be done at render time to avoid duplicate loops
	for( var i = 0; i < results.length; i++ ){
		var result = results[ i ];
		if( result.type == 1 ){
			pageIndex1++;
		} else {
			pageIndex2++;
		}
	}

	return results;
}

// build the test data
populateResults();

// print the test data for verification
console.info( searchResultSet1 );
console.info( searchResultSet2 );

// get the first page of results
var page1 = getNext();
console.info( page1 );

// get the second page of results
var page2 = getNext();
console.info( page2 );
